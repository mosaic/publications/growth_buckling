#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       utils.trilayer_analysis
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Annamaria Kiss <annamaria.kiss@inria.fr>
#
#       File maintainer(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import re
import os
import pickle
import json
import pandas as pd
import numpy as np
import fenics as fe

from scipy.signal import find_peaks
from bvpy.utils.post_processing import SolutionExplorer
from utils.trilayer_geometry import extract_pts_evolution

def read_simulation_data(simulation_file):
    """ Open raw data from simulation (.h5 file + .pkl file)
        Notice that the metadata (from pickle file) will be automaticaly loaded.

        Parameters
        ----------
        simulation_file : str
            The path to the .h5 simulation file

        Return
        ------
        mesh: fe.Mesh
            FeniCS mesh
        cdata : fe.MeshFunctionSizet
            labels for the domains
        bdata : fe.MeshFunctionSizet
            labels for the boundaries
        solutions : dict of fe.Function
            displacement field where keys are the simulation time
        sub_domain_names : dict of str
            the name of the domains
        sub_boundary_names : dict of str
            the name of the boundaries
        time_steps : list
            the time-steps of the simulation
        data_monitoring : dict of dict
            contains different metrics to monitor buckling apparition
    """

    metafile = simulation_file.replace('data.h5', 'metadata.pkl')

    # - Read mesh data
    mesh = fe.Mesh()  # init the mesh
    with fe.HDF5File(fe.MPI.comm_world, simulation_file, 'r') as f:
        f.read(mesh, 'mesh', False)  # fill the mesh with data

    cdata = fe.MeshFunction("size_t", mesh, mesh.topology().dim())
    with fe.HDF5File(fe.MPI.comm_world, simulation_file, 'r') as f:
        f.read(cdata, "cell_labels")

    bdata = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    with fe.HDF5File(fe.MPI.comm_world, simulation_file, 'r') as f:
        f.read(bdata, "boundary_labels")

    ########################################################
    # open metadata
    f = open(metafile, 'rb')
    metadata = pickle.load(f)
    f.close()

    mesh_metadata = metadata['mesh_metadata']  # contains sub_domain_names and sub_boundary_names
    time_steps = metadata['time_steps']
    data_monitoring = metadata['data_monitoring']
    ########################################################

    # - Read simulation results
    V = fe.VectorFunctionSpace(mesh, 'P', 1)  # form the function space
    solutions = {}

    hdf5_file = fe.HDF5File(fe.MPI.comm_world, simulation_file, 'r')
    for ix, time in enumerate(time_steps):
        solution = fe.Function(V)
        dataset_name = f"displacement_{ix}"
        try:
            hdf5_file.read(solution, dataset_name)
            solutions[time] = solution
        except:
            break
    hdf5_file.close()

    return mesh, cdata, bdata, solutions, mesh_metadata, time_steps, data_monitoring


def update_simulation_data(pandas_file, root_directory=None):
    """
    Reads simulation parameters and results from a directory structure,
    checks if they are already present in a pandas DataFrame, and if not,
    adds them and updates the pandas file.

    Parameters
    ----------
    pandas_file: str
        Path to the CSV file containing the DataFrame.
    root_directory: str
        Root directory path to start the search for simulation files.

    Returns:
    None, but updates the pandas_file with new simulation data.
    """

    def convert_json_parameters(json_params):
        """
        Converts simulation parameters from JSON format to a flattened format for comparison with a pandas DataFrame.
        """
        flat_params = {
            'L': json_params['L'],
            'cs': json_params['cs'],
            'Nsteps': json_params['Nsteps'],
            'tmax': json_params['tmax'],
            'imperfection': json_params['imperfection'],
            'geo_pertub_norm': json_params['geo_pertub_norm'],
            'CI_perturbation': json_params['CI_perturbation'],
            # Convert list parameters into separate columns
            'H': json_params['layer_heights'][0],
            'h': json_params['layer_heights'][1],
            'E_i': json_params['E'][0], 'E_m': json_params['E'][1], 'E_o': json_params['E'][2],
            'E_e': json_params['E'][3],
            'C_i': json_params['C'][0], 'C_m': json_params['C'][1], 'C_o': json_params['C'][2],
            'C_e': json_params['C'][3],
            'nu_i': json_params['nu'][0], 'nu_m': json_params['nu'][1], 'nu_o': json_params['nu'][2],
            'nu_e': json_params['nu'][3],
            'g_i': json_params['g'][0], 'g_m': json_params['g'][1], 'g_o': json_params['g'][2],
            'g_e': json_params['g'][3],
            'dg_i': json_params['dg'][0], 'dg_m': json_params['dg'][1], 'dg_o': json_params['dg'][2],
            'dg_e': json_params['dg'][3]
        }
        return flat_params

    def is_simulation_in_dataframe(df, dict_params):
        """
        Check to see if a simulation with specific parameters is already included in the DataFrame.

        Parameters:
        - df: pandas DataFrame containing the parameters and results of simulations.
        - dict_params: Dictionary of parameters

        Returns:
        - True if the simulation is already in the DataFrame, False otherwise.
        """
        # Assert that the dataframe is not empty
        if len(df) == 0:
            return False

        # Select only the relevant columns from the DataFrame
        cols = list(dict_params.keys())
        filtered_df = df[cols].drop_duplicates()

        # Convert to numpy array to increase efficienty
        df_array = filtered_df.to_numpy()
        param_values = np.array(list(dict_params.values()))

        # Check if param_values match any row in unique_params
        return any(np.array_equal(row, param_values) for row in df_array)

    if root_directory is None:
        root_directory = os.getcwd()

    # Read the pandas file
    file_path = os.path.join(root_directory, pandas_file)
    if os.path.exists(file_path) and os.path.getsize(file_path) > 0:
        try:
            df = pd.read_csv(file_path)
        except pd.errors.EmptyDataError:
            print('The file is empty. Initializing an empty DataFrame.')
            df = pd.DataFrame()
    else:
        print('File not found or is empty. Initializing an empty DataFrame.')
        df = pd.DataFrame()

    # Traverse the directory tree
    for root, dirs, files in os.walk(root_directory):
        if 'simulation_parameter.json' in files and 'simulation_metadata.pkl' in files:
            # - Open the JSON file
            with open(os.path.join(root, 'simulation_parameter.json'), 'r') as param_file:
                json_parameters = json.load(param_file)

            # Check if the parameters are already in the DataFrame
            dict_params = convert_json_parameters(json_parameters)

            if not is_simulation_in_dataframe(df, dict_params):
                print(f'Found new data in {os.path.basename(root)}')

                # Load metadata from the .pkl file
                with open(os.path.join(root, 'simulation_metadata.pkl'), 'rb') as result_file:
                    res = pickle.load(result_file)

                # Extract relevant data from metadata
                time_steps = res['time_steps']
                data_monitoring = res['data_monitoring']

                # Prepare and add new rows for each time step
                new_rows = []
                for time in time_steps:
                    new_row = dict_params.copy()
                    new_row.update({
                        'time': time.tolist(),
                        'x_dH': data_monitoring[time]['dH']['x'].tolist(),
                        'dH': data_monitoring[time]['dH']['y'].tolist(),
                        'x_kappa_inner': data_monitoring[time]['kappa_inner']['x'].tolist(),
                        'kappa_inner': data_monitoring[time]['kappa_inner']['y'].tolist(),
                        'x_kappa_outer': data_monitoring[time]['kappa_outer']['x'].tolist(),
                        'kappa_outer': data_monitoring[time]['kappa_outer']['y'].tolist(),
                        'x_kappa_middle': data_monitoring[time]['kappa_middle']['x'].tolist(),
                        'kappa_middle': data_monitoring[time]['kappa_middle']['y'].tolist(),
                    })
                    new_rows.append(new_row)

                # Convert the list of dictionaries to a DataFrame and append it to the existing DataFrame
                df_new_rows = pd.DataFrame(new_rows)
                df_new_rows = df_new_rows.astype({col: 'float32' for col in df_new_rows.columns
                                                  if df_new_rows[col].dtype == 'float64'})
                df_new_rows = df_new_rows.astype({col: 'int32' for col in df_new_rows.columns
                                                  if df_new_rows[col].dtype == 'int64'})
                df = pd.concat([df, df_new_rows], ignore_index=True)

    # Step 4: Rewrite the pandas file
    df.to_csv(file_path, index=False)

    return df

def read_csv_simulation(pandas_file, root_directory=None):
    """
    Reads a CSV file into a pandas DataFrame, converting strings with numerical values to numpy arrays.

    Parameters:
    -----------
    pandas_file: str
        The name of the CSV file, including the extension (e.g., 'data.csv').
    root_directory: str, optional
        The directory from which to read the CSV file. Defaults to the current working directory if None.

    Returns:
    --------
    pd.DataFrame:
        A DataFrame with the CSV data, where columns with string-encoded numbers are converted to numpy arrays.
        Returns None if the file does not exist or is empty.

    Example:
    --------
        df = read_csv_simulation('data.csv')
    """
    if root_directory is None:
        root_directory = os.getcwd()

    # Read the pandas file
    file_path = os.path.join(root_directory, pandas_file)
    if os.path.exists(file_path) and os.path.getsize(file_path) > 0:
        try:
            df = pd.read_csv(file_path)
        except pd.errors.EmptyDataError:
            print('The file is empty.')
            return
    else:
        print('The file was not found.')
        return

    # Convert type:object to np.array
    for col in df.columns:
        if df[col].dtype == 'object':
            # Tenter de convertir chaque cellule en utilisant une expression régulière pour extraire les nombres
            df[col] = df[col].apply(lambda x: np.array(re.findall(r'[-+]?\d*\.?\d+(?:[eE][-+]?\d+)?', x), dtype=float) if isinstance(x, str) else x)

    return df

def compute_mean_curvature_and_extrema(pandas_file, kappa_rel_height=0.05, dh_rel_height=0.1,
                                       root_directory=None, col_names = 'kappa_'):
    """
    Computes mean curvature and identifies the number of extrema (maxima and minima) in curvature data.

    This function reads a specified CSV file into a DataFrame, computes the mean curvature for each specified
    column, and counts the number of extrema in the curvature and height difference ('dH') data.

    Parameters:
    -----------
    pandas_file: str
        The name of the CSV file to process.
    kappa_rel_height: float, optional
        The relative prominence height for identifying maxima/minima in curvature data. Default is 0.05.
    dh_rel_height: float, optional
        The relative prominence height for identifying maxima/minima in height difference data ('dH'). Default is 0.1.
    root_directory: str, optional
        The directory from which to read the CSV file. If None, uses the current working directory.
    col_names: str , optional
    The common starting string for the curvature columns to be analyzed. Default is 'kappa_'.

    Returns:
        pd.DataFrame: The DataFrame with additional columns for mean curvature and counts of maxima and minima.

    Example:
        df = compute_mean_curvature_and_extrema('data.csv')
    """
    # Read the dataframe
    df = read_csv_simulation(pandas_file, root_directory)

    # Get the columns corresponding to curvature
    kappa_cols = [col for col in df.columns if col.startswith(col_names)]

    # Compute mean curvature and add new column
    for col in kappa_cols:
        # - Compute mean curvature
        df["mean_" + col[6:]] = df[col].apply(lambda x: np.mean(x))

        # Count number of maxima
        df[f'num_maxima_{col[6:]}'] = df[col].apply(lambda x: len(find_peaks(x, prominence=kappa_rel_height)[0]))

        # Count number of minima
        df[f'num_minima_{col[6:]}'] = df[col].apply(lambda x: len(find_peaks(-x, prominence=kappa_rel_height)[0]))

    # Count number of maxima
    df[f'num_maxima_dH'] = df['dH'].apply(lambda x: len(find_peaks(x, prominence=dh_rel_height)[0]))

    # Count number of minima
    df[f'num_minima_dH'] = df['dH'].apply(lambda x: len(find_peaks(-x, prominence=dh_rel_height)[0]))

    return df


def compute_var_height(inner_ref, outer_ref, inner_current, outer_current):
    """
    Calculate the variation in height between the referential and current states of the inner and outer points.

    Parameters:
    ----------
    inner_ref : ndarray
        The coordinates of inner points in the referential state.
    outer_ref : ndarray
        The coordinates of outer points in the referential state.
    inner_current : ndarray
        The coordinates of inner points in the current state.
    outer_current : ndarray
        The coordinates of outer points in the current state.

    Returns:
    -------
    ndarray
        The difference in height between the current and referential states.
    """
    h_ref = np.sqrt(np.sum((outer_ref - inner_ref) ** 2, axis=1))
    h_current = np.sqrt(np.sum((outer_current - inner_current) ** 2, axis=1))

    return h_current - h_ref

def compute_curvature(x, reference_point=(0, 0)):
    """
    Compute the curvature of a set of points.

    Parameters:
    ----------
    x : ndarray
        The coordinates of the points to compute the curvature.
    reference_point : tuple, optional
        The reference point for orienting the curvature sign.

    Returns:
    -------
    ndarray
        The curvature values for the set of points.
    """
    # Format reference point
    reference_point = np.array(reference_point)

    # First and second derivatives
    dx = np.gradient(x, axis=0)
    ddx = np.gradient(dx, axis=0)

    # Avoid division by zero in curvature calculation
    denom_curvature = np.sum(dx ** 2, axis=1)
    curvature = (dx[:, 0] * ddx[:, 1] - dx[:, 1] * ddx[:, 0]) / np.power(denom_curvature + np.finfo(float).eps, 1.5)

    # Tangent vectors, avoiding division by zero
    denom_tangent = np.sqrt(np.sum(dx ** 2, axis=1))[:, np.newaxis]
    t = dx / (denom_tangent + np.finfo(float).eps)
    dt = np.gradient(t, axis=0)

    # Normal vectors, avoiding division by zero
    denom_normal = np.sqrt(np.sum(dt ** 2, axis=1))[:, np.newaxis]
    n = dt / (denom_normal + np.finfo(float).eps)

    # Re-orient the normals
    sign_dp = np.sign(np.sum((reference_point - x) * n,
                             axis=1))  # sign of the dot-product between each (reference_point, point) and normals
    sign_curv = np.sign(curvature)  # sign of the curvature

    if np.median(sign_dp != sign_curv) == 1:
        curvature = -curvature  # invert curvature sign : same sign between curvature and sign of dot-product

    return curvature


def monitor_bucking(displacement_field, L=10, H=5, N=50):
    """
    Monitor the buckling of a material by computing the variation in height according to a given displacement field.
    Compute also the curvature of inner and outer surfaces as well as the middle of the material in the current
    state (after application of the displacement field).

    Parameters:
    ----------
    displacement_field : fenics.Function
        A FEniCS function representing the displacement field solution
    L : float
        The length of the material.
    H : float
        The height of the material.
    N : int
        The number of points to discretize the material length.

    Returns:
    -------
    dict of dict
        Contains the variation of height (dH & x-position) as well as the curvature (kappa & x-position) at three
        different height (inner, outer and at the middle of the material)
    """
    # - REFERENCE_PTS (for curvature sign)
    REFERENCE_PTS = [L / 2, H / 3]  # just below the barycenter (for curvature at the middle line of the material)

    # - Compute the curvature of inner, outer and middle points in current state : Do not use interpolation !
    bvpy_sol = SolutionExplorer(displacement_field)

    # compute the variation of height (use interpolation from fenics)
    x_dH = np.linspace(0, L, N)
    inner_pts_ref = np.vstack([[x, 0] for x in x_dH]) # pts of inner surface at reference state
    outer_pts_ref = np.vstack([[x, H] for x in x_dH]) # pts of outer surface at reference state
    inner_pts_current = extract_pts_evolution(displacement_field, inner_pts_ref) # pts of inner surface at current state
    outer_pts_current = extract_pts_evolution(displacement_field, outer_pts_ref) # pts of outer surface at current state
    dH = compute_var_height(inner_pts_ref, outer_pts_ref, inner_pts_current, outer_pts_current) # compute dH

    # compute curvature of inner, outer and middle points in current state
    ref_pts_current = displacement_field(REFERENCE_PTS)  # get the reference_pts in current state

    def monitor_curvature(bvpy_sol, y_target=0, x_min=0, x_max=10, reference_point=(0, 0)):
        xy, disp = bvpy_sol.geometric_filter(axe='y', threshold=y_target, comparator='==', return_position=True, sort='x')
        x_mask = (xy[:, 0] >= x_min) * (xy[:, 0] <= x_max)
        xy_current = xy[x_mask, :] + disp[x_mask, :] # coordinates in current state

        kappa = compute_curvature(xy_current, reference_point=reference_point) # compute curvature

        return xy[x_mask, 0], kappa

    x_inner, kappa_inner = monitor_curvature(bvpy_sol, y_target=0, x_min=0, x_max=L, reference_point=ref_pts_current)
    x_outer, kappa_outer = monitor_curvature(bvpy_sol, y_target=H, x_min=0, x_max=L, reference_point=ref_pts_current)
    x_middle, kappa_middle = monitor_curvature(bvpy_sol, y_target=H/2, x_min=0, x_max=L, reference_point=ref_pts_current)

    # format output
    data_monitoring = {'dH': {'x': x_dH.astype('float16'), 'y': dH.astype('float16')},
                       'kappa_inner': {'x': x_inner.astype('float16'), 'y': kappa_inner.astype('float16')},
                       'kappa_outer': {'x': x_outer.astype('float16'), 'y': kappa_outer.astype('float16')},
                       'kappa_middle': {'x': x_middle.astype('float16'), 'y': kappa_middle.astype('float16')}}

    return data_monitoring