#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       utils.__init__
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Annamaria Kiss <annamaria.kiss@inria.fr>
#
#       File maintainer(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

"""
Package utils - Provides utility classes and functions for morphoelasticity analysis.

This package includes modules for geometric calculations, material properties,
formulation of variational problems, and visualization tools.
"""

__version__ = '1.0.0'

# Defining what gets imported with 'from utils import *'
__all__ = [
    'trilayer_analysis',
    'trilayer_geometry',
    'trilayer_material',
    'trilayer_vform',
    'trilayer_visu',
]