#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       utils.trilayer_material
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Annamaria Kiss <annamaria.kiss@inria.fr>
#
#       File maintainer(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import numpy as np
from bvpy.utils.pre_processing import HeterogeneousParameter
def isotropic_elastic_tensor(E, nu):
    """
    Build the elasticity tensor for an isotropic material in voigt notation assuming plane stress condition.
    The isotropic material is defined by two parameters : young modulus & poisson ratio

    Parameters
    ----------
    E: float
        young modulus
    nu: float
        poisson ratio

    Returns
    -------
    np.ndarray
        The elasticity tensor in voigt notation (3x3 matrix for a 2D material)
    """
    # - Elastic tensor in voigt notation (plane stress)
    H_voigt = (E / ((1 + nu) * (1 - nu))) * np.array([[1, nu, 0],
                                                      [nu, 1, 0],
                                                      [0, 0, (1 - nu) / 2]])
    return H_voigt

def set_ElasticityTensor(geo, E, nu):
    """
    Sets heterogeneous elasticity tensor for the domains defined in geometry geo assuming that each domains are made
    of an isotropic material. Isotropic material is defined by two parameters : young moduli & poisson ratio.

    Parameters
    ----------
    geo: bvpy.domain.AbstractDomain
        The bvpy geometry object containing the mesh and possible other metadat (labels, label's name, ...)
    E: list
        young moduli per domain
    nu: list
        poisson ratio per domain

    Returns
    -------
    bvpy.utils.pre_processing.HeterogeneousParameter
        The elasticity tensor values on the mesh

    Note
    ----
    The elasticity tensor is stored in voigt notation (3x3 matrix for a 2D material)
    See isotropic_elastic_tensor function.
    """
    # Use the HeterogeneousParameter class to apply different H_values or Fg_values according to the sub-domains
    # It will return a FeniCS Coefficient object where coefficients are specific to sub-domains.
    N = len(E)
    label_list = list(np.array(range(1, N + 1)))
    tensor_list = [isotropic_elastic_tensor(E[i], nu[i]) for i in range(N)]
    label_dict = dict(zip(label_list, tensor_list))
    H_values = HeterogeneousParameter(geo.cdata, label_dict)
    return H_values

def growth_tensor(g):
    """
    Build the growth tensor corresponding to a uni-directional growth in direction 0.

    Parameters
    ----------
    g: float
        growth in direction 0

    Returns
    -------
    np.ndarray
        The growth tensor with g growth in direction 0.
    """
    Fg_values_domain = np.array([[g, 0],
                                 [0, 1]])
    return Fg_values_domain


def growth_rate(dg):
    """
    Build the growth rate tensor corresponding to a uni-directional growth rate in direction 0.

    Parameters
    ----------
    g: float
        growth rate in direction 0

    Returns
    -------
    np.ndarray
        The growth rate tensor with g growth in direction 0.
    """
    G_values_domain = np.array([[dg, 0],
                                [0, 0]])
    return G_values_domain


def set_GrowthTensor(geo, g):
    """
    Sets heterogeneous growth tensor parameters for the domains defined in geometry geo
    It considers growth in direction 0, no growth in direction 1

    Parameters
    ----------
    geo: bvpy.domain.AbstractDomain
        The bvpy geometry object containing the mesh and possible other metadat (labels, label's name, ...)
    dg: list
        intrinsic growth rate elements in direction 0

    Returns
    -------
    bvpy.utils.pre_processing.HeterogeneousParameter
        The growth tensor values on the mesh
    """
    N = len(g)
    label_list = list(np.array(range(1, N + 1)))
    tensor_list = [growth_tensor(g[i]) for i in range(N)]
    label_dict = dict(zip(label_list, tensor_list))
    Fg_values = HeterogeneousParameter(geo.cdata, label_dict)
    return Fg_values


def set_GrowthRate(geo, dg):
    """
    Sets heterogeneous growth rate tensor parameters for the domains defined in geometry geo
    It considers growth in direction 0, no growth in direction 1

    Parameters
    ----------
    geo: bvpy.domain.AbstractDomain
        The bvpy geometry object containing the mesh and possible other metadat (labels, label's name, ...)
    dg: list
        intrinsic growth rate elements in direction 0

    Returns
    -------
    bvpy.utils.pre_processing.HeterogeneousParameter
        The growth rate tensor values on the mesh
    """
    N = len(dg)
    label_list = list(np.array(range(1, N + 1)))
    tensor_list = [growth_rate(dg[i]) for i in range(N)]
    label_dict = dict(zip(label_list, tensor_list))
    G_values = HeterogeneousParameter(geo.cdata, label_dict)
    return G_values


def set_ScalarParameter(geo, P):
    """
    Sets heterogeneous scalar parameters for the domains defined in geometry geo

    Parameters
    ----------
    geo: bvpy.domain.AbstractDomain
        The bvpy geometry object containing the mesh and possible other metadat (labels, label's name, ...)
    P: list
        scalar parameter for each domains.

    Returns
    -------
    bvpy.utils.pre_processing.HeterogeneousParameter
        The scalar values on the mesh
    """
    N = len(P)
    label_list = list(np.array(range(1, N + 1)))
    label_dict = dict(zip(label_list, P))
    G_values = HeterogeneousParameter(geo.cdata, label_dict)
    return G_values