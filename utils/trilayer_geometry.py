#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       utils.trilayer_geometry
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Annamaria Kiss <annamaria.kiss@inria.fr>
#
#       File maintainer(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

from bvpy.domains.primitives import Rectangle
import fenics as fe
import numpy as np


def create_trilayer_rounded(L=2, H=1, h=0.1, cell_type='triangle', cell_size=0.1, algorithm='Frontal-Delaunay'):
    """
    Creates a 2D trilayer structure with rounded ends. The structure consists of a central
    'mesophyll' domain, with 'inner' and 'outer' sub-domains on the top and bottom, respectively,
    capped with 'edges' on the left and right sides.

                           / ------------------------------------- \
                         /  |              Outer     (h)          |  \
                        |   +-------------------------------------+   |
                        | e |                                     | e |
                        | d |                                     | d |
                        | g |              Mesophyll  (H)         | g |
                        | e |                                     | e |
                        | s |                                     | s |
                        |   +-------------------------------------+   |
                         \  |               Inner    (h)          | /
                           \ -----------------------------------  /

                            <--------------- L ------------------>

    Parameters:
    ----------
    L : float
        The length of the layers
    H : float
        The height of the mesophyll layer
    h : float
        The height of the inner and outer layer
    cell_type : str
        The type of element used for meshing (e.g., 'triangle').
    cell_size : float
        The target size of the mesh elements.
    algorithm : str
        The meshing algorithm to use (e.g., 'Frontal-Delaunay').

    Returns:
    -------
    geometry : bvpy.AbstractDomain
        The resulting geometry with the mesh and labeled regions.
        Use visualize(geometry, visu_type='domain') to show the labeled regions (from bvpy.utils.visu_pyvista)
    """
    ### - Create the domain
    tol = cell_size / 1e2

    ### - Create the central rectangle
    geometry = Rectangle(length=L, width=2 * h + H, cell_type=cell_type, cell_size=cell_size, algorithm=algorithm,
                         dim=2, clear=True)

    def fragment_with_line(geo, x_start, x_end):
        """Fragment geometry surfaces using a line defined by two points"""
        pts1 = geo.factory.addPoint(x=x_start[0], y=x_start[1], z=0)
        pts2 = geo.factory.addPoint(x=x_end[0], y=x_end[1], z=0)
        line = geo.factory.addLine(startTag=pts1, endTag=pts2)
        geo.factory.fragment(objectDimTags=geometry.factory.getEntities(dim=2), toolDimTags=[(1, line)])

        return geo

    def create_rounded_edge(geo, R, r, center, direction=+1, tol=1e-3):
        """Add rounded edges defined by a center and rwo radius (small and large)."""
        # - small circle
        center_pts = geo.factory.addPoint(x=center[0], y=center[1], z=0)
        start_pts = geo.factory.getEntitiesInBoundingBox(xmin=center[0] - tol, ymin=center[1] - r - tol, zmin=0 - tol,
                                                         xmax=center[0] + tol, ymax=center[1] - r + tol, zmax=0 + tol,
                                                         dim=0)[0]
        end_pts = geo.factory.getEntitiesInBoundingBox(xmin=center[0] - tol, ymin=center[1] + r - tol, zmin=0 - tol,
                                                       xmax=center[0] + tol, ymax=center[1] + r + tol, zmax=0 + tol,
                                                       dim=0)[0]
        if direction > 0:
            small_circle = geo.factory.addCircleArc(start_pts[1], center_pts, end_pts[1])
        else:
            small_circle = geo.factory.addCircleArc(end_pts[1], center_pts, start_pts[1])

        # - large circle
        start_pts = geo.factory.getEntitiesInBoundingBox(xmin=center[0] - tol, ymin=center[1] - R - tol, zmin=0 - tol,
                                                         xmax=center[0] + tol, ymax=center[1] - R + tol, zmax=0 + tol,
                                                         dim=0)[0]
        end_pts = geo.factory.getEntitiesInBoundingBox(xmin=center[0] - tol, ymin=center[1] + R - tol, zmin=0 - tol,
                                                       xmax=center[0] + tol, ymax=center[1] + R + tol, zmax=0 + tol,
                                                       dim=0)[0]
        if direction > 0:
            large_circle = geo.factory.addCircleArc(start_pts[1], center_pts, end_pts[1])
        else:
            large_circle = geo.factory.addCircleArc(end_pts[1], center_pts, start_pts[1])

        # - Create the surfaces
        top_line = geo.factory.getEntitiesInBoundingBox(xmin=center[0] - tol, ymin=center[1] + r - tol, zmin=0 - tol,
                                                        xmax=center[0] + tol, ymax=center[1] + R + tol, zmax=0 + tol,
                                                        dim=1)[0]
        bottom_line = geo.factory.getEntitiesInBoundingBox(xmin=center[0] - tol, ymin=center[1] - R - tol, zmin=0 - tol,
                                                           xmax=center[0] + tol, ymax=center[1] - r + tol, zmax=0 + tol,
                                                           dim=1)[0]
        curve_loop = geo.factory.addCurveLoop([bottom_line[1], small_circle, top_line[1], large_circle])
        geo.factory.addPlaneSurface([curve_loop])

        # - Fill the 'hole' between edge and rectangle
        line = geo.factory.getEntitiesInBoundingBox(xmin=center[0] - tol, ymin=center[1] - r - tol, zmin=0 - tol,
                                                    xmax=center[0] + tol, ymax=center[1] + r + tol, zmax=0 + tol,
                                                    dim=1)[0]
        curve_loop = geo.factory.addCurveLoop([small_circle, line[1]])
        geo.factory.addPlaneSurface([curve_loop])

        return geometry

    ### - Fragment the rectangle to create the inner and outer part
    geometry = fragment_with_line(geometry, x_start=[0, h], x_end=[L, h])  # inner part
    geometry = fragment_with_line(geometry, x_start=[0, H + h], x_end=[L, H + h])  # outer part

    ### - Create two rounded edges (half circle)
    geometry = create_rounded_edge(geometry, R=H / 2 + h, r=H / 2, center=[0, h + H / 2], direction=+1, tol=tol)
    geometry = create_rounded_edge(geometry, R=H / 2 + h, r=H / 2, center=[L, h + H / 2], direction=-1, tol=tol)

    ### - Add vertical line at the middle of the object (boundary conditions)
    geometry = fragment_with_line(geometry, x_start=[L / 2, 0], x_end=[L / 2, H + 2 * h])  # inner part

    ### - Add horyzontal line at the middle of the object (boundary conditions)
    geometry = fragment_with_line(geometry, x_start=[0, H / 2 + h], x_end=[L, H / 2 + h])  # inner part

    # - Synchronize
    geometry.factory.synchronize()

    ### - Set some labels
    geometry.model.addPhysicalGroup(dim=2, tags=[8, 9], tag=1)
    geometry.model.setPhysicalName(dim=2, tag=1, name='Inner')

    geometry.model.addPhysicalGroup(dim=2, tags=[5, 7, 12, 13, 14, 15], tag=2)
    geometry.model.setPhysicalName(dim=2, tag=2, name='Mesophyll')

    geometry.model.addPhysicalGroup(dim=2, tags=[10, 11], tag=3)
    geometry.model.setPhysicalName(dim=2, tag=3, name='Outer')

    geometry.model.addPhysicalGroup(dim=2, tags=[4, 6], tag=4)
    geometry.model.setPhysicalName(dim=2, tag=4, name='Edge')

    ### - Discretize
    geometry.discretize()
    return geometry


def add_geometric_perturbation(geo, geo_pertub_norm):
    """
    Applies a geometric perturbation to the vertices of a given geometry.

    This function adds a random displacement to each vertex of the mesh within the geometry.
     The displacement vectors are normalized and scaled by the provided `geo_pertub_norm`.

    Parameters:
    ----------
    geo : bvpy.AbstractDomain
        The geometric object containing the mesh to be perturbed.
    geo_pertub_norm : float
        The normalization factor for the perturbation, controlling the magnitude of the displacement.

    Returns:
    -------
    None
    """
    dim = geo.mesh.geometric_dimension()

    # - Add a random displacement at each vertices
    V_pertub = fe.VectorFunctionSpace(geo.mesh, 'Lagrange', 1, dim=2)
    u_pertub = fe.Function(V_pertub)

    u_array = u_pertub.vector().get_local()
    s = (len(u_array) // dim, dim)

    # Generate random vectors
    for i in range(s[0]):
        vec = np.random.rand(dim) - 0.5
        norm_vec = geo_pertub_norm * vec / np.linalg.norm(vec)
        u_array[i * dim:(i + 1) * dim] = norm_vec

    # Assign normalized vector to the function
    u_pertub.vector().set_local(u_array)
    u_pertub.vector().apply('insert')

    # Now, apply the geometric perturbation to the mesh (inplace)
    update_mesh(geo.mesh, u_pertub, geo.cdata, inplace=True)

    return


def find_cell_closest(mesh, target_point):
    """
    Finds the cell in a mesh that is closest to a given target point.

    The function computes the Euclidean distance between the barycenter of each cell in the mesh and a target point,
     returning the index of the closest cell.

    Parameters:
    ----------
    mesh : dolfin.Mesh
        The mesh to search within.
    target_point : array-like
        The target point to find the closest cell to.

    Returns:
    -------
    int
        The index of the closest cell to the target point.
    """
    # Compute distance between barycenters and the given point
    barycentres = np.array([cell.midpoint().array() for cell in fe.cells(mesh)])
    distances = np.linalg.norm(barycentres - target_point, axis=1)

    return np.argmin(distances)


def find_cells_in_domain(mesh, bottom_left_corner, top_right_corner):
    """
    Identifies cells within a specified rectangular domain of a mesh.

    This function checks all cells in the mesh and returns the indices of those whose barycenters lie within
    the rectangle defined by the corner points `bottom_left_corner` and `top_right_corner`.

    Parameters:
    ----------
    mesh : dolfin.Mesh
        The mesh whose cells are to be checked.
    bottom_left_corner : array-like
        The coordinates of the bottom-left corner of the rectangular domain.
    top_right_corner : array-like
        The coordinates of the top-right corner of the rectangular domain.

    Returns:
    -------
    list of int
        A list of cell indices that lie within the specified rectangular domain.
    """
    # Calculate the barycenters of each cell in the mesh
    barycenters = np.array([cell.midpoint().array() for cell in fe.cells(mesh)])
    # Initialize an empty list to store indices of cells within the domain
    indices_in_domain = []

    # Iterate over each barycenter to check if it lies within the defined rectangle
    for index, barycenter in enumerate(barycenters):
        # Check if the barycenter is within the bounds of the rectangle
        if (bottom_left_corner[0] <= barycenter[0] <= top_right_corner[0] and
                bottom_left_corner[1] <= barycenter[1] <= top_right_corner[1]):
            indices_in_domain.append(index)

    return indices_in_domain


def change_cell_label(geo, index, label1, label2):
    """
    Changes the label of a cell in a geometric object if it matches a specified label.

    Parameters:
    ----------
    geo : bvpy.AbstractDomain
        The geometric object containing the mesh and cell data.
    index : int
        The index of the cell whose label is to be changed.
    label1 : int
        The original label that needs to be matched for the change to occur.
    label2 : int
        The new label to be assigned to the cell.

    Returns:
    -------
    None
    """
    assert geo.cdata.array()[index] == label1
    geo.cdata.set_value(index, label2)
    return


def add_perturbation_to_solution(solution_field, max_norm=0.01, v_idx=None, vertical=False):
    """
    Adds a random perturbation to a solution field. The perturbation can be applied globally to the
     solution field or localized to a specific vertex. The perturbation can also be constrained to be vertical.

    Parameters:
    ----------
    solution_field : dolfin.Function
        The solution field to which the perturbation is to be added.
    max_norm : float
        The maximal magnitude of the perturbation.
    v_idx : int, optional
        The index of the vertex in the mesh where the perturbation is to be added.
        If None, perturbation is applied globally. Default is None.
    vertical : bool, optional
        If True, perturbation is only applied in the vertical direction. Default is False.

    Returns:
    -------
    dolfin.Function
        The solution field with the added perturbation.
    """
    V = solution_field.function_space()
    mesh = V.mesh()
    dim = mesh.geometric_dimension()
    perturbation = fe.Function(V)
    values = perturbation.vector().get_local()

    if v_idx is not None:
        # Localized perturbation at a specific vertex
        pert = np.random.uniform(-1, 1, dim)
        if vertical:
            pert[0] = 0  # Zero out the horizontal component for vertical perturbation
        #norm = np.linalg.norm(pert)
        pert *= max_norm #/ norm
        values[v_idx * dim:(v_idx + 1) * dim] = pert
    else:
        # Global perturbation at all vertices
        for i in range(0, len(values), dim):
            pert = np.random.uniform(-1, 1, dim)
            if vertical:
                pert[0] = 0  # Zero out the horizontal component for vertical perturbation
            #norm = np.linalg.norm(pert)
            pert *= max_norm #/ norm
            values[i:i + dim] = pert

    perturbation.vector().set_local(values)
    perturbation.vector().apply('insert')

    # Add the perturbation to the solution field
    solution_field.vector()[:] += perturbation.vector()[:]

    return solution_field


def extract_pts_evolution(displacement_field, pts):
    """
    Extracts the evolution of a set of points under a given displacement solution.
    Given a reference set of points and a displacement field, this function computes the new positions of
     the points after the displacement is applied.

    Parameters:
    ----------
    displacement_field : dolfin.Function
        The displacement field to be applied to the points.
    pts : list or array-like
        The reference points before displacement.

    Returns:
    -------
    numpy.ndarray
        The new positions of the points after applying the displacement.
    """
    return np.vstack([x + displacement_field(x) for x in pts])


def update_mesh(mesh, displacement, boundaries, inplace=False):
    """
    Updates a mesh according to a given displacement field.
    This function moves the nodes of the mesh according to the specified displacement field. It can operate in place,
     modifying the original mesh, or create a new mesh with updated geometry.

    Parameters:
    ----------
    mesh : dolfin.Mesh
        The original mesh to be updated.
    displacement : dolfin.Function
        The displacement field describing the movement of each node.
    boundaries : dolfin.MeshFunction
        The boundary conditions associated with the mesh.
    inplace : bool, optional
        If True, the original mesh is updated in place; otherwise, a new mesh is created. Default is False.

    Returns:
    -------
    tuple
        A tuple containing the updated mesh and boundary conditions. If `inplace` is True, the original objects are
         returned after modification.
    """
    if inplace:
        fe.ALE.move(mesh, displacement)
        return mesh, boundaries
    else:
        #displacement.set_allow_extrapolation(True)
        new_mesh = fe.Mesh(mesh)
        new_boundaries = fe.MeshFunction("size_t", new_mesh, mesh.geometric_dimension())
        new_boundaries.set_values(boundaries.array())
        fe.ALE.move(new_mesh, displacement)

        return new_mesh, new_boundaries
