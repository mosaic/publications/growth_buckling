#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       utils.trilayer_visu
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Annamaria Kiss <annamaria.kiss@inria.fr>
#
#       File maintainer(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import pyvista as pv
import fenics as fe
import numpy as np
import matplotlib.pyplot as plt

from matplotlib.colors import ListedColormap
from bvpy.utils.visu_pyvista import visualize
from utils.trilayer_geometry import update_mesh
from utils.trilayer_vform import MorphoElasticity

# SERVER PYVISTA
#pv.start_xvfb() # Uncomment to display

# Constants for scalar bar appearance in plots
SCALAR_STRAIN_BAR_ARGS = dict(title_font_size=12,
                              label_font_size=10,
                              n_labels=3,
                              italic=True,
                              fmt="%.1e")
SCALAR_CDATA_BAR_ARGS = dict(label_font_size=9,
                             title_font_size=12,
                             height=0.25,
                             vertical=True,
                             position_x=0.8,
                             position_y=0.01)

# Color mapping for different domains in the visualization
CMAP_DOMAINS = {'Mesophyll':  (240, 228, 66, 255),
                'Outer': (0, 114, 178, 255),
                'Inner': (213, 94, 0, 255),
                'Edge': (0, 0, 0, 255)}

# Mapping from label to domain name for visualization
LAB_TO_DOMAINS = {1: 'Inner',
                  2: 'Mesophyll',
                  3: 'Outer',
                  4: 'Edge'}

def plot_list_strain(displacement_fields, subspace=0, filename=None, Ncol=5, val_range=None):
    """
    Plot strain fields derived from displacement fields at different times.

    Parameters
    ----------
    displacement_fields: dict
        A dictionary with keys as times and values as displacement fields.
    subspace: int
        The subspace to visualize. Default is 0.
    filename: str
        The name of the file to save the plot. Default is None.
    Ncol: int
        The number of columns in the subplot. Default is 5.
    val_range: tuple
        The range of values to use for the color scale. Default is None.

    Returns
    -------
    pv.Plotter or None
        This function saves the generated plot to a file if filename is not None. Else, it returns the pv.Plotter.
    """
    # Compute the number of rows needed for the subplot layout
    times = list(displacement_fields.keys())
    N_fields = len(times)
    Nrow = (N_fields) // Ncol + 1

    plotter = pv.Plotter(shape=(Nrow, Ncol), window_size=[200 * Ncol, 200 * Nrow])
    for j in range(0, Nrow):
        for i in range(0, Ncol):
            ix = j * Ncol + i
            if ix < N_fields:
                plotter.subplot(j, i)

                # - Compute strain from the displacement field
                vform = MorphoElasticity()
                strain = vform.strain(displacement_fields[times[ix]])

                # - Visualize the strain field
                visualize(strain.sub(subspace), show_plot=False, plotter=plotter,
                          val_range=val_range, actor_kwargs={'scalar_bar_args': SCALAR_STRAIN_BAR_ARGS})
                plotter.add_title(f"t={times[ix]}", font_size=10)

    # Link cameras across all subplots, set the view direction, and save the figure
    plotter.link_views()
    plotter.set_viewup([0, 1, 0])

    if filename is not None:
        plotter.save_graphic(filename)
        return
    else:
        plotter.show()
        return plotter

def plot_list_strain_trace(displacement_fields, filename=None, Ncol=5, val_range=None):
    """
    Plot strain trace fields derived from displacement fields at different times.

    Parameters
    ----------
    displacement_fields: dict
        A dictionary with keys as times and values as displacement fields.
    filename: str
        The name of the file to save the plot. Default is None.
    Ncol: int
        The number of columns in the subplot. Default is 5.
    val_range: tuple
        The range of values to use for the color scale. Default is None.

    Returns
    -------
    pv.Plotter or None
        This function saves the generated plot to a file if filename is not None. Else, it returns the pv.Plotter.
    """
    # Compute the number of rows needed for the subplot layout
    times = list(displacement_fields.keys())
    N_fields = len(times)
    Nrow = (N_fields) // Ncol + 1

    plotter = pv.Plotter(shape=(Nrow, Ncol), window_size=[200 * Ncol, 200 * Nrow])
    for j in range(0, Nrow):
        for i in range(0, Ncol):
            ix = j * Ncol + i
            if ix < N_fields:
                plotter.subplot(j, i)

                # Compute the trace of the strain field
                vform = MorphoElasticity()
                strain = vform.strain(displacement_fields[times[ix]])
                FS = fe.FunctionSpace(strain.function_space().mesh(), 'DG', 0)
                trace_strain = fe.project(fe.tr(strain), FS)

                # Visualize the trace of the strain field
                visualize(trace_strain, show_plot=False, plotter=plotter,
                          val_range=val_range, actor_kwargs={'scalar_bar_args': SCALAR_STRAIN_BAR_ARGS})
                plotter.add_title(f"t={times[ix]}", font_size=10)

    # Link cameras across all subplots, set the view direction, and save the figure
    plotter.link_views()
    plotter.set_viewup([0, 1, 0])

    if filename is not None:
        plotter.save_graphic(filename)
        return
    else:
        plotter.show()
        return plotter
def plot_list_deformation(mesh, domains, displacement_fields, filename=None, Ncol=5, resolution_factor = 1):
    """
    Plot deformed meshes at different times (from displacement fields)

    Parameters
    ----------
    mesh: fe.Mesh
        The original mesh before deformation.
    domains: fe.MeshFunction
        The function defining different domains in the mesh.
    displacement_fields: dict of fe.Function
        A dictionary with keys as times and values as displacement fields.
    filename: str
        The name of the file to save the plot. Default is None.
    Ncol: int
        The number of columns in the subplot. Default is 5.

    Returns
    -------
    pv.Plotter or None
        This function saves the generated plot to a file if filename is not None. Else, it returns the pv.Plotter.
    """
    # Parameters for scalar_bar
    scalar_bar_args = SCALAR_CDATA_BAR_ARGS.copy()
    scalar_bar_args['label_font_size'] *= resolution_factor

    # Compute the number of rows needed for the subplot layout
    times = list(displacement_fields.keys())
    N_fields = len(times)
    Nrow = (N_fields - 1) // Ncol + 1

    # Create a color map for the domains
    ordered_color = [np.array(CMAP_DOMAINS[k]) / 255 for k in
                     sorted(LAB_TO_DOMAINS.values())]  # Correct color order
    cmap = ListedColormap(np.vstack(ordered_color))  # Create colormap

    plotter = pv.Plotter(shape=(Nrow, Ncol), window_size=[200 * Ncol * resolution_factor, 200 * Nrow * resolution_factor])
    for j in range(0, Nrow):
        for i in range(0, Ncol):
            ix = j * Ncol + i
            if ix < N_fields:
                plotter.subplot(j, i)

                # - Deform domains (MeshFunctionSize) according to the displacement field
                _, domain_moved = update_mesh(mesh, displacement_fields[times[ix]], domains)

                # Visualize the deformed mesh colored by domain
                visualize(domain_moved, show_edges=False, plotter=plotter, show_plot=False,
                          cmap=cmap, dict_values=LAB_TO_DOMAINS, scalar_bar_title='',
                          actor_kwargs={'scalar_bar_args': scalar_bar_args.copy()})
                plotter.add_title(f"t={times[ix]}", font_size=10*resolution_factor)

    # Link cameras across all subplots, set the view direction, and save the figure
    plotter.link_views()
    plotter.set_viewup([0, 1, 0])

    if filename is not None:
        plotter.save_graphic(filename)
        plotter.clear()
        plotter.close()
        del plotter
        return
    else:
        plotter.show()
        return plotter

def plot_mesh_and_solutions(mesh=None, cdata=None, displacement_fields=None):
    """
     Plot mesh, domain and displacement fields (solutions) at different times

     Parameters
     ----------
     mesh: fe.Mesh
         The original mesh before deformation.
     domains: fe.MeshFunction
         The function defining different domains in the mesh.
     displacement_fields: dict of fe.Function
         A dictionary with keys as times and values as displacement fields.

     Returns
     -------
     pv.Plotter
     """

    Ncol = 0

    if mesh is not None:
        Ncol += 1

    if cdata is not None:
        Ncol += 1

    if displacement_fields is not None:
        Ncol += len(displacement_fields)

    # Create a color map for the domains
    ordered_color = [np.array(CMAP_DOMAINS[k]) / 255 for k in
                     sorted(LAB_TO_DOMAINS.values())]  # Correct color order
    cmap = ListedColormap(np.vstack(ordered_color))  # Create colormap

    plotter = pv.Plotter(shape=(1, Ncol), window_size=[220 * Ncol, 250])

    ix = 0
    if mesh is not None:
        plotter.subplot(0, ix)
        visualize(mesh, plotter=plotter, show_plot=False)
        plotter.add_title('Mesh', font_size=10)
        ix += 1

    if cdata is not None:
        plotter.subplot(0, ix)
        visualize(cdata, plotter=plotter, show_edges=False, show_plot=False,
                  cmap=cmap, dict_values=LAB_TO_DOMAINS, scalar_bar_title='',
                  actor_kwargs={'scalar_bar_args': SCALAR_CDATA_BAR_ARGS.copy()})  # cdata : fe.MeshFunctionSize
        plotter.add_title('Tissue domains', font_size=10)
        ix += 1

    if displacement_fields is not None:
        for time, solution in displacement_fields.items():
            plotter.subplot(0, ix)
            visualize(solution, plotter=plotter, show_plot=False, show_mesh=False,
                      actor_kwargs={'scalar_bar_args': SCALAR_STRAIN_BAR_ARGS})  # solutions : dict of fe.Function
            plotter.add_title(f'Deformation field\n(t={time})', font_size=10)
            ix += 1

    plotter.link_views()
    plotter.view_xy()
    plotter.show()

    return plotter


def plot_buckling_monitoring(data_monitoring, save_path=None):
    """
    Generates a series of plots to monitor the emergence of buckling effects by visualizing the variation of
    thickness and curvatures at different tissue layers.

    This function creates four subplots in a single figure: one for the delta-height (variation of thickness)
    and three for the curvature variations (inner, middle, outer) of the tissue. Each subplot displays data
    across different time points to monitor the development of buckling effects.

    The `data_monitoring` dict corresponds to one of the output of trilayer_analysis.read_simulation_data.

    Parameters
    ----------
    data_monitoring: dict of dict
        A dictionary where each key is a time point, and each value is another dictionary
        with keys 'dH', 'kappa_inner', 'kappa_middle', and 'kappa_outer'. Each of these
        contains two keys, 'x' and 'y', which provide the data points to be plotted.
    save_path: str, optional
    The path where the figure should be saved. If not provided, the figure is shown directly.

    Returns
    -------
    fig: matplotlib.figure.Figure
        The figure object is returned only if no save_path is provided. If a save_path is provided,
        the figure is saved to that path, and no object is returned.
    """
    # Visu buckling monitoring
    fig = plt.figure(figsize=(12, 4))

    ax1 = fig.add_subplot(1, 4, 1)
    ax2 = fig.add_subplot(1, 4, 2)
    ax3 = fig.add_subplot(1, 4, 3, sharey=ax2)
    ax4 = fig.add_subplot(1, 4, 4, sharey=ax3)

    fig.subplots_adjust(wspace=0.4)

    # - For each iterations, get the delta-height
    for time, dd in data_monitoring.items():
        ax1.plot(dd['dH']['x'], dd['dH']['y'], label=f't={time}')
        ax2.plot(dd['kappa_inner']['x'], dd['kappa_inner']['y'], label=f't={time}')
        ax3.plot(dd['kappa_middle']['x'], dd['kappa_middle']['y'], label=f't={time}')
        ax4.plot(dd['kappa_outer']['x'], dd['kappa_outer']['y'], label=f't={time}')

    ax1.set_xlabel('x-axis')
    ax2.set_xlabel('x-axis')
    ax3.set_xlabel('x-axis')
    ax4.set_xlabel('x-axis')

    ax1.set_ylabel(r'$\Delta h$')
    ax2.set_ylabel(r'$\kappa_{inner}$')
    ax3.set_ylabel(r'$\kappa_{middle}$')
    ax4.set_ylabel(r'$\kappa_{outer}$')

    handles, labels = ax1.get_legend_handles_labels()
    fig.legend(handles, labels, loc='center right')

    if save_path is not None:
        fig.savefig(save_path, dpi=220)
        plt.close(fig)
        return
    else:
        fig.show()
        return fig
