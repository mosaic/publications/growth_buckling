#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       utils.trilayer_vform
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Annamaria Kiss <annamaria.kiss@inria.fr>
#
#       File maintainer(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

from bvpy.vforms.elasticity import ElasticForm
import fenics as fe
import numpy as np

class MorphoElasticity(ElasticForm):
    """
    A class to model and solve morphoelastic problems, where growth-induced deformations are followed by
    elastic relaxations in hyperelastic materials. This class utilizes an energy density function that is
    quadratic with respect to the Green-Lagrange strain tensor, accommodating both small and large deformation
    scenarios based on the alpha parameter.

    The energy density function is defined as:
        W = 0.5 * Jg * (Ee : H : Ee)
    where:
        - Jg is the determinant of the growth deformation tensor (Fg),
        - Ee is the Green-Lagrange strain tensor,
        - H is the fourth-order elastic tensor.

    Attributes:
    ----------
    Fg : np.ndarray
        The growth part of the deformation gradient tensor.
    H : np.ndarray
        The fourth-order elastic tensor in Voigt notation.
    alpha : float, optional
        A parameter ranging from 0 to 1, controlling the deformation linearity. A value of 0 corresponds to a
        linear (small-deformation) problem, whereas 1 indicates a non-linear (finite-strain) problem.
        Default is 1.
    q_degree: int, optional
        The quadrature degree for integration points in numerical computations. Default is 10.

    Methods
    -------
    tensor2_to_voigt(tensor, weight=False)
        Converts a symmetric rank 2 tensor to its Voigt notation.
    voigt_to_tensor2(voigt, weight=False)
        Converts a Voigt notation vector back to a symmetric rank 2 tensor.
    zeta(u)
        Computes the incremental elastic deformation tensor.
    _strain(u)
        Calculates the Green-Lagrange strain tensor considering the non-linearity parameter alpha.
    W(u)
        Defines the strain energy density function based on the Green-Lagrange strain tensor.
    construct_form(u, v, sol)
        Constructs the variational form for the morphoelastic problem.
    set_expression()
        Sets the expression for the energy functional.
    """

    def __init__(self, Fg=np.diag([1, 1]), H=np.diag([1, 1, 1]), alpha=1, q_degree=10):
        super().__init__()

        self.set_parameters(Fg=Fg)
        self.set_parameters(H=H)
        self.set_parameters(alpha=alpha)

        self.q_degree = q_degree # quadrature_degree for the integration points

    def tensor2_to_voigt(self, tensor, weight=False):
        """
        Convert a 2D or 3D rank 2 tensor to its Voigt notation if symmetric tensor T:
            T_ij = T_ji,

        Parameters:
        tensor (numpy.ndarray): The tensor to convert (either 3x3 or 2x2).
        weight (bool) : If a weight need to be apply to recover isometry mapping (ex: if Hooke's Law)

        Returns:
        numpy.ndarray: The tensor in Voigt notation (either 1x6 or 1x3).
        """
        if weight:
            w = 2
        else:
            w = 1

        if tensor.ufl_shape == (3, 3):  # 3D case
            voigt = fe.as_vector(
                [tensor[0, 0], tensor[1, 1], tensor[2, 2], w * tensor[1, 2], w * tensor[0, 2], w * tensor[0, 1]])
        elif tensor.ufl_shape == (2, 2):  # 2D case
            voigt = fe.as_vector([tensor[0, 0], tensor[1, 1], w * tensor[0, 1]])
        else:
            raise ValueError("Invalid tensor shape. Tensor must be either 3x3 or 2x2.")
        return voigt

    def voigt_to_tensor2(self, voigt, weight=False):
        """
        Convert a Voigt notation matrix to a 2D or 3D rank 2 tensor assuming a symmetric tensor T:
            T_ij = T_ji

        Parameters:
        voigt (numpy.ndarray): The Voigt notation matrix to convert (either 1x6 or 1x3).
        weight (bool) : If a weight need to be apply to recover isometry mapping (ex: if Hooke's Law)

        Returns:
        numpy.ndarray: The converted tensor (either 3x3 or 2x2).
        """
        if weight:
            voigt[3:] = voigt[3:] / 2

        if voigt.ufl_shape == (6,):  # 3D case
            tensor = fe.as_tensor([[voigt[0], voigt[5], voigt[4]],
                                   [voigt[5], voigt[1], voigt[3]],
                                   [voigt[4], voigt[3], voigt[2]]])
        elif voigt.ufl_shape == (3,):  # 2D case
            tensor = fe.as_tensor([[voigt[0], voigt[2]],
                                   [voigt[2], voigt[1]]])
        else:
            raise ValueError("Invalid Voigt vector length. Length must be either 6 for 3D or 3 for 2D.")
        return tensor

    # - Zeta function : elastic deformation increment
    def zeta(self, u):
        d = u.geometric_dimension()
        Fg = self._parameters['Fg']
        return fe.grad(u) * fe.inv(Fg) + fe.inv(Fg) - fe.Identity(d)

    # - Green-Lagrange strain tensor
    def _strain(self, u):
        alpha = self._parameters['alpha']
        return fe.sym(self.zeta(u)) + 0.5 * alpha * self.zeta(u).T * self.zeta(u)

    # - Strain energy density function
    def W(self, u):
        # - Strain energy density function
        Fg = self._parameters['Fg']
        H_voigt = self._parameters['H']

        Ee_tensor = self._strain(u)
        Ee_voigt = self.tensor2_to_voigt(Ee_tensor,
                                         weight=True)  # rewrite the Green-Lagrange strain tensor in voigt notation
        HEe = self.voigt_to_tensor2(fe.dot(H_voigt,
                                           Ee_voigt))  # compute the tensor product H : Ee in voigt notation and convert in tensor notation
        return 0.5 * fe.det(Fg) * fe.inner(HEe, Ee_tensor)

    def construct_form(self, u, v, sol):
        self.lhs = self.W(sol) * self.dx(metadata={"quadrature_degree": self.q_degree})
        self.lhs = fe.derivative(self.lhs, sol, v)

    def set_expression(self):
        self._expression = f'psi(u)dx'