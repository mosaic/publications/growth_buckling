#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       run_multiprocessing_buckling_simulation.py
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Annamaria Kiss <annamaria.kiss@inria.fr>
#
#       File maintainer(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import json
import numpy as np
import itertools
import subprocess
from multiprocessing import Pool

N_CORES = 10 # adapt to your system

# Definition of the parameters to explore
Ei = np.logspace(0, 6, 7) # inner young modulus
Eo = np.logspace(0, 6, 7) # outer young modulus
E = [[ei, 1, eo, 1e6] for ei in Ei for eo in Eo] # list of list of young modulus

total_height = 5
layer_heighs = [[total_height - 2 * i * 0.1, i * 0.1] for i in [1, 2, 3, 4]] # list of list of layer heights

PARAMETER_EXPLORER = {
    'E': E,
    'layer_heights': layer_heighs,
}

# Generate unique combinations of parameters
params_keys = sorted(PARAMETER_EXPLORER)
params_combinations = list(itertools.product(*(PARAMETER_EXPLORER[param] for param in params_keys)))

def run_simulation(comb):
    # Construct dict of parameters for this combination
    param_combinations = dict(zip(params_keys, comb))

    # Convert in JSON chain
    params_json = json.dumps(param_combinations)

    # Run script
    subprocess.run(['python', 'script_buckling_simulation.py', '--params', params_json])

if __name__ == '__main__':
    with Pool(processes=N_CORES) as pool:
        pool.map(run_simulation, params_combinations)
