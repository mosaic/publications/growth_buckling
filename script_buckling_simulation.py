#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       script_buckling_simulation.py
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Annamaria Kiss <annamaria.kiss@inria.fr>
#
#       File maintainer(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

# Script to simulate morphoelastic growth using FEniCS and additional libraries
import os
import json
import pickle
import argparse
import numpy as np
import scipy as sp
import fenics as fe

from utils.trilayer_material import set_ElasticityTensor, set_GrowthRate, set_GrowthTensor, set_ScalarParameter
from utils.trilayer_geometry import (create_trilayer_rounded, find_cells_in_domain, change_cell_label,
                                     add_geometric_perturbation, add_perturbation_to_solution)
from utils.trilayer_analysis import monitor_bucking
from utils.trilayer_visu import plot_list_deformation, plot_buckling_monitoring
from utils.trilayer_vform import MorphoElasticity

from bvpy.bvp import BVP
from bvpy.boundary_conditions import dirichlet, Boundary

DEFAULT_PARAMETER = {
    'L': 10, # Length of the domain
    'layer_heights': [4.8, 0.1], # Height of the layers (mesophyll, inner/outer)
    'cs': 0.2, # Cell size
    'Nsteps': 3, # Number of time-steps (between [0, tmax])
    'tmax': 3, # Maximum time
    'E': [1e6, 1, 1e4, 1e6], # Young modulus of layer (inner, mesophyll, outer, edge)
    'C': [0.01, 0.1, 0.01, 0.01], # Extensibility of layer (inner, mesophyll, outer, edge)
    'nu': [0.49]*4, # Poisson coefficients of layer (inner, mesophyll, outer, edge)
    'g': [1]*4, # Initial growth tensor (inner, mesophyll, outer, edge)
    'dg': [0, 0, 0.1, 0], # Growth rate (inner, mesophyll, outer, edge)
    'imperfection': 0, # Flag to add geometric imperfection
    'geo_pertub_norm': 0, # Norm of geometric perturbation
    'CI_perturbation': 1e-4, # Norm of perturbation of initial condition
    'threshold_peak_curvature': 0.05, # Threshold for the detection of peak (auto-detection of buckling)
    'dirname': None, # Output folder name to save data
    'root_name': None, # Optional root name to save data
    'adapt_time_steps': True, # static or dynamic (default) time steps
    'plot_figures': True # plot or not visualizations of the simulation (deformations & buckling monitoring)
}

# Solver parameters
SOLVER_PARAMETERS = {
    'krylov_solver': {'absolute_tolerance': 1E-13,
                      'relative_tolerance': 1E-12,
                      'maximum_iterations': 1000,
                      'error_on_nonconvergence': True,
                      },
    'preconditioner': 'default',
    'linear_solver': 'superlu', # 'mumps' can be used to speed up calculations (without running parameter exploration)
    'maximum_iterations': 100,
    'line_search': 'basic',
    'absolute_tolerance': 1e-9,
    'relative_tolerance': 1e-9,
    'report': True,
}

# Parse optional arguments
parser = argparse.ArgumentParser(description="""
Buckling simulation in a trilayer material with morphoelastic growth.
Parameters should be passed as a JSON string. Here are the accepted parameters with their default values:

- 'L': Domain length (default: 10).
- 'layer_heights': Heights of the layers [mesophyll, inner/outer] (default: [4.8, 0.1]).
- 'cs': Cell size for meshing (default: 0.2).
- 'Nsteps': Number of time steps (default: 6).
- 'tmax': Maximum time (default: 3).
- 'E': Young's modulus of the layers [inner, mesophyll, outer, edge] (default: [1e6, 1, 1e4, 1e6]).
- 'C': Extensibility of the layers [inner, mesophyll, outer, edge] (default: [0.01, 0.1, 0.01, 0.01]).
- 'nu': Poisson's ratios of the layers [inner, mesophyll, outer, edge] (default: [0.49, 0.49, 0.49, 0.49]).
- 'g': Initial growth tensor [inner, mesophyll, outer, edge] (default: [1, 1, 1, 1]).
- 'dg': Growth rate [inner, mesophyll, outer, edge] (default: [0, 0, 0.1, 0]).
- 'imperfection': Flag to add geometric imperfection (default: 0).
- 'geo_pertub_norm': Norm of geometric perturbation (default: 0).
- 'CI_perturbation': Initial condition perturbation (default: 1e-4).
- 'threshold_peak_curvature': Threshold for the detection of peak of curvature in outer surface (default: 0.05).
- 'dirname': Output folder name to save data. If None, an automatic folder name is generated (default: None).
- 'root_name' : Root to create and store the output folder (default: None).
- 'adapt_time_steps': Use or not an adaptative time-steps (default: True).
- 'plot_figures':  plot or not visualizations of the simulation (deformations & buckling monitoring) (default: True).

Examples of usage:

To run the simulation with default parameters:
python script_buckling_simulation.py

To run the simulation with a specific domain length and Young's modulus values:
python script_buckling_simulation.py --params '{"L": 20, "E": [2e6, 2, 2e4, 2e6]}'

To add a geometric imperfection and modify the growth rate:
python script_buckling_simulation.py --params '{"imperfection": 1, "dg": [0.05, 0.05, 0.15, 0.05]}'
""", formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--params', type=str, help='Parameter as JSON chain form.', default='{}')
args = parser.parse_args()

# Load parameters from the JSON chain and update the default values
parametres_utilisateur = json.loads(args.params)
parameter = {**DEFAULT_PARAMETER, **parametres_utilisateur}

# Geometry parameters
L = parameter['L']
layer_heights = parameter['layer_heights']
H, h = layer_heights
cs = parameter['cs']

# Geometric imperfection
imperfection_width = cs * 2
imperfection_height = cs
imperfection = parameter['imperfection']
geo_pertub_norm = parameter['geo_pertub_norm']
CI_perturbation = parameter['CI_perturbation']

# Material parameters
E = parameter['E']
nu = parameter['nu']
g = parameter['g']
dg = parameter['dg']
C = parameter['C']

# Time parameters
Nsteps = parameter['Nsteps']
tmin = 0
tmax = parameter['tmax']
initial_dt = (tmax - tmin) / Nsteps # get the initial dt
dt = initial_dt # set dt
adapt_time_steps = parameter['adapt_time_steps'] # use or not adaptative time steps (False : constant time-steps)

# Directory for output
dirname = parameter['dirname']
if dirname is None:
    dirname = f"SimulationOutput_E{E}_C{C}_dg{dg}_L{L}_h{layer_heights}_cs{cs}_CI{CI_perturbation}"

root = parameter['root_name']
if root is None:
    root = dirname + "/"
else:
    root = root + "/" + dirname + "/"
    
os.makedirs(root, exist_ok=True)

# Check if the simulation have already been done
needToBeDone = True
if os.path.exists(root+"monitoring_buckling.pdf"):
    print('\nSimulation already be done !')
    needToBeDone = False

if needToBeDone:
    # Save parameters in JSON file
    parameter['dirname'] = dirname
    with open(root + 'simulation_parameter.json', 'w') as f:
        json.dump(parameter, f, indent=4)

    # Geometry creation and mesh perturbation
    geo = create_trilayer_rounded(L=L, H=H, h=h, cell_type='triangle', cell_size=cs, algorithm='Frontal-Delaunay')
    if imperfection:
        A = [L / 2 - imperfection_width / 2, H + h - imperfection_height]
        B = [L / 2 + imperfection_width / 2, H + h]
        cell_list = find_cells_in_domain(geo.mesh, A, B)
        for ind in cell_list:
            change_cell_label(geo, ind, 2, 3)
    if geo_pertub_norm > 0:
        add_geometric_perturbation(geo, geo_pertub_norm)

    # Save mesh and label data
    hdf5_filename = root + "simulation_data.h5"
    with fe.HDF5File(fe.MPI.comm_world, hdf5_filename, 'w') as hdf5_file:
        hdf5_file.write(geo.mesh, "mesh")
        hdf5_file.write(geo.cdata, "cell_labels")
        hdf5_file.write(geo.bdata, "boundary_labels")

    # Material properties assignment
    H_values = set_ElasticityTensor(geo, E, nu)
    Fg_values = set_GrowthTensor(geo, g)
    ImposedGrowthRate = set_GrowthRate(geo, dg)
    C_values = set_ScalarParameter(geo, C)

    # Boundary conditions
    bc_bottomPoint_Fixed = [dirichlet(val=[0, 0], boundary=Boundary("near(x, L/2, cs/10)", cs=cs, L=L) &
                                                           Boundary("near(y, 0, cs/10)", cs=cs),
                                      method="pointwise")]
    bc_topPoint_SlipVertical = [dirichlet(val=0, subspace=0, boundary=Boundary("near(x, L/2, cs/10)", L=L, cs=cs) &
                                                                      Boundary("near(y, y_top, cs/10)", cs=cs, y_top=H+2*h),
                                          method="pointwise")]
    bc = bc_bottomPoint_Fixed + bc_topPoint_SlipVertical

    ############################# - START SIMULATION - #############################
    # Initialize variables for the loop
    time = tmin
    i = 0
    G2 = None
    convergence_count = 0
    doStorage = True
    prev_solution = None
    prev_Fg = Fg_values
    list_solution, list_Fg, list_info, solver_res_info = {}, {}, {}, []
    VG = fe.TensorFunctionSpace(geo.mesh, 'DG', 0) # will be used to project intermediary results

    # Growth simulation loop
    while time <= tmax:
        time_ = np.around(time, 5) # create a time for display and storage
        print(f"\nStep {i} : t={time_}, dt={dt}\n")

        ### Set the problem for a given iteration
        if time == tmin:
            # Initial problem setup and solve
            vform = MorphoElasticity(H=H_values, Fg=Fg_values)
            problem = BVP(geo, vform, bc)
        else:
            # Compute strain from the previous solution
            vform = MorphoElasticity()
            prev_strain = vform.strain(prev_solution)

            # Update growth tensor and solve for new configuration
            G2 = ImposedGrowthRate + C_values * prev_strain
            Fg_values = fe.project(prev_Fg + dt * G2 * prev_Fg, VG)  # increase the growth tensor and project
            ### Note: projection helps to avoid the use of large number of integration points to solve problem after
            #         large number of iterations

            vform = MorphoElasticity(H=H_values, Fg=Fg_values)
            problem = BVP(geo, vform, bc)

            # Set and add perturbation to initial solution (vertical perturbation)
            sol_with_perturbation = prev_solution.copy(deepcopy=True) # copy in case divergence
            sol_with_perturbation = add_perturbation_to_solution(sol_with_perturbation, max_norm=CI_perturbation, vertical=True)
            problem.solution.interpolate(sol_with_perturbation)

        ### Try to solve the problem and check if solution is admissible
        try:
            solver_res_info = problem.solve(return_solver_info=True, **SOLVER_PARAMETERS) # try to solve the problem

            # Check if the displacement field is physically admissible
            V0 = fe.FunctionSpace(geo.mesh, 'DG', 0)
            detJ = fe.project(fe.det(fe.grad(problem.solution)), V0)  # compute the det. of the Jacobian of the disp.

            if detJ.compute_vertex_values().min() < -0.1:
                print(f"\n\nSolution found is non-physical (det(J) < 0) at time={time_}\n")
                doStorage = False  # divergence, stop the computation
            else:
                doStorage = True  # store this solution
        except:
            print(f"\n\nSolver diverged at time={time_}\n")
            doStorage = False  # do not store this solution

        ### Store the solution and metadata if needed
        if doStorage:
            # - Save the current solution
            prev_solution = problem.solution.copy(deepcopy=True)
            prev_Fg = Fg_values
            convergence_count += 1 # update the convergence count

            list_solution[time_] = prev_solution  # keep only the displacement field
            list_Fg[time_] = prev_Fg
            list_info[time_] = solver_res_info

            with fe.HDF5File(fe.MPI.comm_world, hdf5_filename, 'a') as hdf5_file:
                hdf5_file.write(prev_solution, f"displacement_{i}")

            # Store metadata for post-processing
            monitoring = monitor_bucking(prev_solution, L=L, H=H + 2 * h, N=100)

            if os.path.exists(root + "simulation_metadata.pkl"):
                with open(root + "simulation_metadata.pkl", 'rb') as pkl_file:
                    data = pickle.load(pkl_file)
                # add new data
                with open(root + 'simulation_metadata.pkl', 'wb') as pkl_file:
                    data['data_monitoring'][time_] = monitoring
                    data['solver_residuals'] = list_info
                    data['time_steps'].append(time_)
                    pickle.dump(data, pkl_file)
            else:
                with open(root + "simulation_metadata.pkl", 'wb') as pkl_file:
                    data = {'mesh_metadata': {'sub_domain_names': geo.sub_domain_names,
                                              'sub_boundary_names': geo.sub_boundary_names},
                            'data_monitoring': {time_: monitoring},
                            'solver_residuals': list_info,
                            'time_steps': [time_]}
                    pickle.dump(data, pkl_file)

            # Dooble dt after two succesives convergences and if dt < initial_dt
            if convergence_count >= 2 and dt < initial_dt:
                print(f"Increase dt={dt} --> dt={min(dt * 2, initial_dt)}")
                dt = min(dt * 2, initial_dt)
                convergence_count = 0  # Reset the convergence counter

            i += 1  # update step

        elif adapt_time_steps:
            time -= dt # reset to the previous time

            # A problem appears
            print(f"Reduce dt={dt} --> dt={dt/2}")
            dt /= 2 # reduce the dt
            convergence_count = 0  # Reset the convergence counter

            if dt < (initial_dt / (2 ** 10)):
                break # if dt is too small in comparison to the initial dt, stop the loop
        else:
            break # if no adaptative time-steps, just break the loop

        time += dt # update the time

    ############################# - START ANALYSIS - #############################
    peak_threshold = parameter['threshold_peak_curvature']  # threshold used to detect potential peaks in outer layer

    ############################# - START REPORT - #############################
    # Generate README file to sum up the simulation information (parameters + results)
    with open(root + "simulation_metadata.pkl", 'rb') as pkl_file:
        data = pickle.load(pkl_file)
        data_monitoring = data['data_monitoring']
        time_steps = data['time_steps'] # just in case

    readme_filename = os.path.join(root, "README.md")
    with open(readme_filename, 'w') as readme_file:
        # Analyze the results of the simulation
        last_time_point = max(time_steps)  # Assumer que time_steps contient les points de temps

        readme_file.write("\n# Simulation Results\n\n")

        # Detect peaks in the last available time-points
        x = data_monitoring[last_time_point]['kappa_outer']['y']
        peaks_count = len(sp.signal.find_peaks(x, prominence=peak_threshold)[0])

        # Buckling results depends on the number of peak detected
        if peaks_count >= 1:
            readme_file.write(f"- **Buckling: YES**\n")
        else:
            readme_file.write(f"- **Buckling: NO**\n")

        readme_file.write(f"- Maximal convergence time-steps: {last_time_point}\n")
        readme_file.write(f"- Number of detected peaks: {peaks_count}\n")
        readme_file.write(f"- Height threshold: {peak_threshold}\n")
        readme_file.write(f"- Number iterations: {len(np.hstack(list(list_info.values())))}\n")
        readme_file.write(f"- Iterations details:\n")
        for time, residuals in list_info.items():
            readme_file.write(f"{time}: {len(residuals)} it.\n")

        # Parameters of the simulation
        readme_file.write("\n# Simulation Parameters\n\n")
        readme_file.write(f"## Geometry\n- Length of the domain: {L}\n- Layer heights: {H}, {h}\n- Cell size: {cs}\n\n")
        readme_file.write(
            f"## Material Parameters\n*(inner, mesophyl, outer, edge)*\n- Young's modulus: {E}\n- Extensibility: {C}\n- Poisson's ratios: {nu}\n- Initial growth tensor: {g}\n- Growth rate: {dg}\n\n")
        readme_file.write(
            f"## Simulation\n- Number of time steps: {Nsteps}\n- Maximum time: {tmax}\n- Imperfection flag: {imperfection}\n")
        readme_file.write(
            f"- Geometric perturbation norm: {geo_pertub_norm}\n- Initial condition perturbation: {CI_perturbation}\n\n")

    ############################# - START VISUALIZATION - #############################
    plot_figure = parameter['plot_figures']
    if plot_figure:
        # - Save the evolution of the mesh deformations
        plot_list_deformation(geo.mesh, geo.cdata, list_solution,
                              filename=root+"bilayer_deformation_dg"+str(dg[2])+".pdf",
                              Ncol=5)
        # - Save the monitoring of the buckling
        plot_buckling_monitoring(data_monitoring, save_path=root+"monitoring_buckling.pdf")

