
# Simulation Results

- **Buckling: YES**
- Maximal convergence time-steps: 3.0
- Number of detected peaks: 2
- Height threshold: 0.1
- Number iterations: 37
- Iterations details:
0: 1 it.
1.0: 11 it.
2.0: 11 it.
3.0: 14 it.

# Simulation Parameters

## Geometry
- Length of the domain: 10
- Layer heights: 4.8, 0.1
- Cell size: 0.2

## Material Parameters
*(inner, mesophyl, outer, edge)*
- Young's modulus: [1000000.0, 1, 10000.0, 1000000.0]
- Extensibility: [0.01, 0.1, 0.01, 0.01]
- Poisson's ratios: [0.49, 0.49, 0.49, 0.49]
- Initial growth tensor: [1, 1, 1, 1]
- Growth rate: [0, 0, 0.1, 0]

## Simulation
- Number of time steps: 3
- Maximum time: 3
- Imperfection flag: 0
- Geometric perturbation norm: 0
- Initial condition perturbation: 0.0001

