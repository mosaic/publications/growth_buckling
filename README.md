# Numerical model for a growing layered sepal

This repository contains scripts & dedicated methods to perform the numerical simulations presented in the manuscript ... The content of the repository is available under LGPL licence, if you use it, please cite the above reference.

We adopt the framework of morphoelasticity, assuming that the residual stresses are solely created by a local growth deformation tensor, and that the observed deformation gradient can be decomposed multiplicatively in the elastic and the growth deformation tensors. In this framework we consider an energy density that is quadratic in the Green-Lagrange strain tensor. We consider here a trilayer structure with rounded edges, that represents a simplified transversal section of a growing sepal. Regarding our hypothesis on growth, we consider here that the growth rate has both an imposed target component, that is along the layers, as well as a component that stands for an influence of the elastic strain on growth. For more details on the theoretical formulation of the model, we refer to the manuscript.

In order to find the equilibrium deformations at each time step we use the Finite Element Method. The implementation is done using the computing platform [FeniCS](https://fenicsproject.org/) with its Python interface, with a special use of the [Bvpy Python library](https://gitlab.inria.fr/mosaic/bvpy).


![Image](fig/buckling.png "Time evolution of a growing trilayer with buckling outer surface.")

## Dependencies, download and installation

You can download the content of the repository using for instance the `git` command line tool
```bash
git clone https://gitlab.inria.fr/mosaic/publications/growth_buckling.git
cd growth_buckling
```

To run the codes or pipelines in this repository, it is necessary to install FeniCS, bvpy, and a number of other Python packages. An adapted conda environment can be automatically set using the YAML file `buckling_env.yaml`. In order to do that, please install CONDA on your system:
(https://conda.io/projects/conda/en/latest/user-guide/install), and then create the environment by

```bash
conda env create --file buckling_env.yaml
```

The created environment is called `buckling_env` and to activate it, use

```bash
conda activate buckling_env
```

## Content of the repository

- script for one simulation: `script_buckling_simulation.py`
- script for a parameter space exploration: `run_multiprocessing_buckling_simulation.py`
- simulation results used for the manuscript: `data/buckling_parameter_exploration.csv`
- jupyter notebook for visualizations and data analysis: `Analysis_of_buckling_simulations.ipynb`
- functions used in the above tools are gathered in the directory `utils`


## One simulation

To run the simulation with the default parameters:

```bash
python script_buckling_simulation.py
```

To access the help of the script:

```bash
python script_buckling_simulation.py -h
```

To run the simulation with custom parameters:

```bash
python script_buckling_simulation.py --params '{"L": 20, "E": [2e6, 2, 2e4, 2e6]}'
```

### Parameters

#### Geometry

**L** - Width of the trilayer part (default: 10)

**layer_heights** - Mesophyll and epidermal layer thickness [hm, h] (default: [4.8, 0.1])

#### Elasticity

**E** - Young's moduli for the inner, mesophyll, outer, and edge layers respectively [Ei, Em, Eo, Eend] (default: [1e6, 1, 1e4, 1e6])

**nu** - Poisson's ratio for the inner, mesophyll, outer, and edge layers respectively [nui, num, nuo, nuend] (default: [0.49, 0.49, 0.49, 0.49])

#### Growth

**g** - Initial growth for the inner, mesophyll, outer, and edge layers respectively [gi, gm, go, gend] (default: [1, 1, 1, 1])

**dg** - Growth rate for the inner, mesophyll, outer, and edge layers respectively [dgi, dgm, dgo, dgend] (default: [0, 0, 0.1, 0])

**C** - Extensibility of the inner, mesophyll, outer, and edge layers respectively [Ci, Cm, Co, Cend] (default: [0.01, 0.1, 0.01, 0.01])

#### Time

**tmax** - Maximum time for the simulation (default: 3)

**Nsteps** - Number of time steps between [0, `tmax`] for the simulation. The time steps of the simulation is  defined by `Nsteps/tmax` (default: 3)

**adapt_time_steps** - Indicates whether adaptive time steps should be used. If `True` the time steps can be decreased in case of divergence of the solver. The time step can also be increased (with a maximum defined by the initial time steps) if the solver converges twice in a row (default: True)


#### Numerics

**cs** - Cell (triangle) size used in meshing (default: 0.2)

**imperfection** - A flag to add geometric imperfection to the simulation. Imperfection is added by changing some mesophyll cells into outer cells close from the outer layer (default: False) 

**geo_pertub_norm** - Norm of geometric perturbation. Geometric perturbation consists of moving randomly each vertex of the mesh by a displacement of norm `geo_pertub_norm` (default: 0)

**CI_perturbation** - Perturbation applied to the initial conditions for the nonlinear finite element solver. Perturbation is added to each dof (default: 1e-4)

#### Path for output

**dirname** - Name of the folder to save the simulation data. If `None` the `dirname` is set automatically as the current directory (default: None)

**root_name** - Root directory for creating and storing the output folder (default: None)

#### Plot and output analysis

**plot_figures** - Determines whether to plot visualizations of the simulation (default: True)

**threshold_peak_curvature** - Threshold for detecting the peak of curvature on the outer surface. This parameter is only used to give insight for users of the presence of buckling (default: 0.1)




## Exploration of the parameter space

The parameter space exploration can be performed in a parallelized manner using multiprocessing 

```bash
python run_multiprocessing_buckling_simulation.py
```

Parameters to explore need to be defined as a dictionnary in the `run_multiprocessing_buckling_simulation.py` script file.

```bash
PARAMETER_EXPLORER = {
  'L' : [10, 20], # two combinations of domain length
  'Nsteps': [3, 6], # two combinations of Nsteps 
  'E': [[1e4, 1, 1e4, 1e6],
        [1e4, 1, 1e5, 1e6],
        [1e4, 1, 1e6, 1e6]], # three combinaisons of young modulus 
}
```

Note that running a lot of simulations can take a long computation time.

## Vizualisation and data analysis

The notebook `Analysis_of_buckling_simulations.ipynb` contains examples of vizualisation of the results of one simulation as well as those of the parameter space exploration presented in the manuscript. In order to use the notebook launch the jupyter notebook application in the installed and activated `buckling_env` environment,
```bash
jupyter notebook
```
